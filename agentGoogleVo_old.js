const path = require('path')
const Nightmare = require('nightmare');
//require('nightmare-real-mouse')(Nightmare);
const co = require('co');
//import parallelLimit from 'async/parallelLimit';
const queue = require('async-co/queue');


var localurl = path.resolve(__dirname, 'index.html')


/*var dashboard = function*() {

 let nightmare = Nightmare({
 show: true,
 waitTimeout: 120000, // in ms
 alwaysOnTop: false,
 switches: {
 'emulate-touch-events': true,
 'proxy-server': ''
 },
 });

 //THIS GONNA BLOW
 var concurrency = ''
 var numbers = ''

 yield nightmare
 .goto('file://' + localurl)
 .wait(20000) //WARNING
 .evaluate(function () {
 return {
 'concurrency': document.querySelector('#concurrency').value,
 'numbers': document.querySelector('#numbers').value
 }
 })
 .then((value) => {
 concurrency = value.concurrency
 numbers = value.numbers
 //console.log(value);
 })
 yield nightmare.end()

 /!*.wait()*!/
 ///.end()
 //.on('page', (type, message, stack)=>{console.log(type,message,stack);})
 //.end()
 //console.log(concurrency, numbers);
 return {
 'concurrency': Number.parseInt(concurrency),
 'numbers': numbers.split('\n')
 }
 }*/

var agent = function *(tasks) {
    var profile = function*() {
        yield {
            first_name: ['carlos', 'daniel', 'pedro', 'luis'].randomElement(),
            last_name: ['perez', 'gutierrez', 'arias'].randomElement(),
            email: Math.floor(Math.random() * 1000000).toString(),
            passwd: ['aswv', 'wxa', 'SXd', 'TsW'].randomElement() + Math.floor(Math.random() * 1000000).toString(),
            birth_day: (Math.floor(Math.random() * 30) + 1).toString(),
            birth_month: '',
            birth_year: '1977',
            phone: '',
        }
    }
    let user = profile().next().value
    console.log("User: " + user + " tasks: " + tasks);
    //console.log("tasks" + tasks);
    //for (var i = 0; i < numbers.length; i++) {
    let numbers = tasks
    console.log([user.first_name, user.last_name, user.email].join(' '));
    //console.log(numbers);
    //console.log("Numbers loaded!")


    let nightmare = Nightmare({
        show: true,
        waitTimeout: 120000, // in ms
        alwaysOnTop: false,
        switches: {
            'emulate-touch-events': true,
            'proxy-server': ''
        },
    });

    yield nightmare
        .useragent([
            'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36',
            'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko',
            'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A'
            ].randomElement()
        )
        //.wait(1000000)
        .goto('https://accounts.google.com/SignUp?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ltmpl=default')
        //.inject('js','jquery.js')
        //.inject('js','VirtualPointer.js')
        .type('#FirstName', user.first_name)
        .type('#LastName', user.last_name)
        .type('#GmailAddress', user.first_name + user.last_name + user.email)
        .type('#Passwd', user.passwd)
        .type('#PasswdAgain', user.passwd)
        .type('#BirthDay', '03')
        .type('#BirthYear', '1971')
        .type('#RecoveryPhoneNumber', '')
        .type('#RecoveryPhoneNumber', numbers.slice(0, 1))
        .evaluate(function () {
            document.getElementById('BirthMonth').setAttribute("aria-invalid", "false");
            document.getElementById('HiddenBirthMonth').setAttribute('value', '03');
            document.getElementById('Gender').setAttribute("aria-invalid", "false");
            document.getElementById('HiddenGender').setAttribute('value', 'MALE');
            return 0;
        })
        .click('#submitbutton')
        .wait('#tos-scroll-button')
        .click('#iagreebutton')
        .wait('#signupidvinput')
        //.exists('')
        .wait(500)
    for (var i = 0; i < numbers.length; i++) {
        console.log(numbers[i]);
        for (var time = 1; time <= 2; time++) {
            console.log("try N°: " + time);
            yield nightmare
                .type('#signupidvinput', '')
                .wait(1500)
                .type('#signupidvinput', numbers[i])
                .click('#signupidvmethod-voice')
                .click('#next-button')
                .wait(4000)
            var error = yield nightmare.visible('span.errormsg');
            //console.log(error)
            if (!error) {
                console.log('Number passed')
                yield nightmare.wait('#signupidv > div.tip > a')
                    .wait(5000)
                    .click('#signupidv > div.tip > a') //turn back
                    .wait(5000);
            }
            else {
                var error_message = yield nightmare.evaluate(function () {
                    return document.getElementsByClassName('errormsg')[0].innerHTML.trim();
                    //return "Este número de teléfono ya se usó muchas veces para fines de verificación."
                });
                switch (error_message) {
                    //number verified too many times
                    case "Este número de teléfono ya se usó muchas veces para fines de verificación.":
                        time = 2;
                        break;
                    //profile number expired
                    case "No se puede usar este número de teléfono para la verificación.":
                        i = numbers.length
                        break;
                }
                console.log('Number expired: ' + error_message)
                yield nightmare.wait(1000)
            }
        }

        //"https://accounts.google.com/UserSignUpIdvChallenge"
    }
    yield nightmare.end()

}

//var fs = require('fs');
//var numbers = fs.readFileSync('./numbers.txt').toString().split("\n");

Array.prototype.randomElement = function () {
    return this[Math.floor(Math.random() * this.length)]
}


//console.log();


function onerror(err) {
    // log any uncaught errors
    // co will not throw any errors you do not handle!!!
    // HANDLE ALL YOUR ERRORS!!!
    console.error(err.stack);
}

var engine = function*(concurrency, numbers) {
    //alert(concurrency+numbers)
    //var parameters = yield dashboard();
    numbers = numbers.map((n) => '+' + n)
    var partitions = [], size = 5;

    while (numbers.length > 0)
        partitions.push(numbers.splice(0, size));

//    alert(concurrency + numbers);

    var q = queue(agent, concurrency)
    yield partitions.map((p) => q.push(p));


    /*var lol = yield [
     agent(partitions[0]),

     //agent(),
     //        agent(partitions[0])
     //agent(profile().next().value, partitions[2]),
     //agent(profile().next().value, partitions[3]),
     //co.wrap(agent)(profile().next().value, partitions[1]),
     ];
     */
    console.log("parameters", concurrency, numbers); // => [1, 2, 3]
}
var main = function (concurrency, tasks) {
    //alert(concurrency + tasks);
    tasks = tasks.split('\n')
    return co.wrap(engine)(concurrency, tasks)
    //return co.wrap(agent)(tasks)
};

module.exports.main = main;

/*vo(partitions.map(p => run(profile().next().value, p)))
 .then(result => console.log(result))
 .catch(err => console.log("ERR: " + err.toString()))*/
/*    let user = profile().next().value
 vo(run)(user,numbers.slice(0, 10), function (err, result) {
 console.log('running')
 console.dir(result);
 });
 */